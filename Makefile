.PHONY: image test docs bundle

IMAGE_NAME ?= codeclimate/codeclimate-rubocop

SLIM_IMAGE_NAME ?= codeclimate/codeclimate-rubocop:slim

image:
	docker build --rm -t $(IMAGE_NAME) .

slim: image
	docker-slim build --tag $(SLIM_IMAGE_NAME) --http-probe=false --exec '/usr/src/app/bin/codeclimate-rubocop || continue' --mount "$$PWD:/code" --workdir '/code' --preserve-path-file 'paths.txt' $(IMAGE_NAME) && prettier --write slim.report.json 

test: slim
	container-structure-test test --image $(IMAGE_NAME) --config tests/container-test-config.yaml && container-structure-test test --image $(SLIM_IMAGE_NAME) --config tests/container-test-config.yaml

test-ruby: image
	docker run --rm $(IMAGE_NAME) sh -c "cd /usr/src/app && bundle install --with=test && bundle exec rake"

docs: image
	docker run --rm \
		--user root \
		--workdir /usr/src/app \
		--volume $(PWD):/usr/src/app \
		$(IMAGE_NAME) sh -c "apk --update add git && bundle install --with=test && bundle exec rake docs:scrape"

bundle:
	docker run --rm \
	  --entrypoint /bin/sh \
	  --volume $(PWD):/usr/src/app \
	  $(IMAGE_NAME) -c "cd /usr/src/app && bundle $(BUNDLE_ARGS)"